/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entornosumlpractica;

/**
 *
 * @author Mel
 */
public class PokemonFuego extends Pokemons implements Ataque {

    private int ataque;

    public PokemonFuego(int ataque, String nombre, String tipo, int tamanio, double peso) {
        super(nombre, tipo, tamanio, peso);
        this.ataque = ataque;
    }

    public int getAtaque() {
        return ataque;
    }

    public void setAtaque(int ataque) {
        this.ataque = ataque;
    }

    public void ataqueevolucion(int ataqueevolucion) {
        setAtaque(ataqueevolucion);
        System.out.println("Su evolucion ha sido realizada");
    }

    @Override
    public String toString() {
        return super.toString() + "PokemonFuego{" + "ataque=" + ataque + '}';
    }

    @Override
    public boolean fuerzaataque(boolean fuerza) {
        return fuerza;
    }

}

