/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entornosumlpractica;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Mel
 */
public class Principal {

    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);
        int pok;
        ArrayList<Pokemons> pokemons = new ArrayList();
        ArrayList<PokemonFuego> fuego = new ArrayList();
        ArrayList<PokemonElectrico> electrico = new ArrayList();

        do {
            System.out.println("1.Insertar Pokemon\n"
                    + "2.Evolucion del pokemon\n"
                    + "3.Pelea entre pokemons. ¿Quién ganará? \n"
                    + "4.Mostrar todos los pokemons\n"
                    + "0.Salir\n"
                    + "Que desea hacer");
            pok = teclado.nextInt();
            switch (pok) {
                case 1:
                    int teeligoati1;
                    System.out.println("1.Pokemon Fuego 2.Pokemon Electrico");
                    teeligoati1 = teclado.nextInt();
                    switch (teeligoati1) {
                        case 1:
                            System.out.println("Dime su nombre");
                            String nombre1 = teclado.next();
                            System.out.println("Dime su tipo");
                            String tipo1 = teclado.next();
                            System.out.println("Dime su tamaño (cm)");
                            int tamanio1 = teclado.nextInt();
                            System.out.println("Dime su peso (kg)");
                            int peso1 = teclado.nextInt();
                            System.out.println("Dime la potencia de su ataque");
                            int ataque1 = teclado.nextInt();
                            PokemonFuego f = new PokemonFuego(ataque1, nombre1, tipo1, tamanio1, peso1);
                            fuego.add(f);
                            pokemons.add(f);
                            break;
                        case 2:
                            System.out.println("Dime nombre");
                            String nombre2 = teclado.next();
                            System.out.println("Dime su tipo");
                            String tipo2 = teclado.next();
                            System.out.println("Dime su tamaño (cm)");
                            int tamanio2 = teclado.nextInt();
                            System.out.println("Dime su peso (kg)");
                            int peso2 = teclado.nextInt();
                            System.out.println("Dime la potencia de su ataque");
                            int ataque2 = teclado.nextInt();
                            PokemonElectrico e = new PokemonElectrico(ataque2, nombre2, tipo2, tamanio2, peso2);
                            electrico.add(e);
                            pokemons.add(e);
                            break;
                        default:
                            System.out.println("Opcion no valida");
                            break;
                    }
                    break;
                case 2:
                    int teeligoati2;
                    System.out.println("1.Pokemon Fuego 2.Pokemon Electrico");
                    teeligoati2 = teclado.nextInt();
                    switch (teeligoati2) {
                        case 1:
                            System.out.println("Dime su nombre");
                            String n1 = teclado.next();
                            System.out.println("Dime su tipo");
                            String tipo = teclado.next();
                            for (int i = 0; i < fuego.size(); i++) {
                                if (fuego.get(i).getNombre().equals(n1) && fuego.get(i).getTipo().equals(tipo)) {
                                    System.out.println("Para evolucionar su ataque pulse 1.");
                                    System.out.println("1.Ataque");
                                    int opcion1 = teclado.nextInt();

                                    if (opcion1 == 1) {
                                        System.out.println("Dime su nueva potencia de ataque");
                                        int ataqueevolucion = teclado.nextInt();
                                        fuego.get(i).ataqueevolucion(ataqueevolucion);
                                    } else if (opcion1 != 1) {
                                        System.out.println("Opcion no válida");
                                    }
                                }
                            }
                            break;
                        case 2:
                            System.out.println("Dime su nombre");
                            String n2 = teclado.next();
                            System.out.println("Dime su tipo");
                            String tipo2 = teclado.next();
                            for (int i = 0; i < electrico.size(); i++) {
                                if (electrico.get(i).getNombre().equals(n2) && electrico.get(i).getTipo().equals(tipo2)) {
                                    System.out.println("Para evolucionar su ataque pulse 1.");
                                    System.out.println("1.Ataque");
                                    int opcion2 = teclado.nextInt();
                                    if (opcion2 == 1) {
                                        System.out.println("Dime su nueva potencia de ataque");
                                        int ataqueevolucion = teclado.nextInt();
                                        electrico.get(i).ataqueevolucion(ataqueevolucion);
                                    } else if (opcion2 != 1) {
                                        System.out.println("Opción no válida");
                                    }
                                }
                            }
                            break;
                    }
                    break;
                case 3:
                    PokemonFuego f2 = new PokemonFuego(0, "", "", 0, 0);
                    PokemonElectrico e2 = new PokemonElectrico(0, "", "", 0, 0);
                    System.out.println("Dime el nombre del Primer Pokémon");
                    String n3 = teclado.next();
                    System.out.println("Dime el nombre del Segundo Pokémon");
                    String n4 = teclado.next();
                    for (int i = 0; i < pokemons.size(); i++) {
                        if (n3.equals(pokemons.get(i).getNombre())) {
                            f2 = (PokemonFuego) pokemons.get(i);
                        }
                        if (n4.equals(pokemons.get(i).getNombre())) {
                            e2 = (PokemonElectrico) pokemons.get(i);
                        }
                    }
                    f2.fuerzaataque(true);
                    if (f2.getAtaque()> e2.getAtaque()) {
                        System.out.println("Victoria de " + f2.getNombre());
                    } else {
                        System.out.println("Victoria de " + e2.getNombre());
                    }
                    break;
                case 4:
                    for (int i = 0; i < pokemons.size(); i++) {
                        System.out.println(pokemons.get(i).toString());
                    }
                    break;
                case 0:
                    System.out.println("Adiós, hasta la próxima aventura!!");
                    break;
                default:
                    System.out.println("Opcion no valida");
                    break;
            }
        } while (pok != 0);

    }
}
