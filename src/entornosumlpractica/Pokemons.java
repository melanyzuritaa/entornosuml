/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entornosumlpractica;

/**
 *
 * @author Mel
 */
public class Pokemons {

    private String nombre;
    private String tipo;
    private int tamanio;
    private double peso;

    public Pokemons(String nombre, String tipo, int tamanio, double peso) {
        this.nombre = nombre;
        this.tipo = tipo;
        this.tamanio = tamanio;
        this.peso = peso;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getTamanio() {
        return tamanio;
    }

    public void setTamanio(int tamanio) {
        this.tamanio = tamanio;
    }

    public double getPeso() {
        return peso;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }

    @Override
    public String toString() {
        return "Pokemons{" + "nombre=" + nombre + ", tipo=" + tipo + ", tamaño=" + tamanio + ", peso=" + peso + '}';
    }
}
